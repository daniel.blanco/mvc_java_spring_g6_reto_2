package com.retos.reto2.controller;

import com.retos.reto2.model.Project;
import com.retos.reto2.services.UserServices;
import com.retos.reto2.services.ProjectServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.retos.reto2.model.User;

import java.awt.print.PrinterJob;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/projects")

public class ProjectController {

    @Autowired
    ProjectServices projectServices;

    //Endpoint para crear un proyecto
    @PostMapping
    public Project saveProject(@RequestBody Project project){

        return projectServices.saveProject(project);
    }

    //Endpoint para listar información de un proyecto
    @GetMapping("/{id}")
    public Optional<Project> getProyectId(@PathVariable Long id){
        return projectServices.getProjectId(id);
    }

    //Endpoint para listar todos los proyectos
    @GetMapping
    public List<Project> getAllProjects(){
        return projectServices.getProjects();
    }


}
