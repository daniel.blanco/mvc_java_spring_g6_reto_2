package com.retos.reto2.controller;

import com.retos.reto2.model.Investment;
import com.retos.reto2.model.Project;
import com.retos.reto2.model.User;
import com.retos.reto2.services.InvestmentServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/investments")

public class InvestmentController {
    @Autowired
    InvestmentServices investmentServices;

    //Endpoint para registrar una Inversión
    @PostMapping
    public ResponseEntity<Object>  saveInvestment(@RequestBody Investment investment){
        return investmentServices.makeInvestment(investment.getUserId(),investment.getProjectId(),investment.getMontoInvertido());
    }

    //Endpoint para listar todas las inversiones
    @GetMapping
    public List<Investment> getAllInvestments(){
        return investmentServices.getInvestments();
    }

    //Endpoint para mostrar inversiones de un userId
    @GetMapping("/{userId}")
    public ResponseEntity<List<Investment>> getInvestmentsByUserId(@PathVariable long userId) {
        List<Investment> investments = investmentServices.getInvestmentsByUserId(userId);

        if (investments.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(investments);
        }
    }


}
