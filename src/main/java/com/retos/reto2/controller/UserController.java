package com.retos.reto2.controller;

import com.retos.reto2.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.retos.reto2.model.User;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    UserServices userServices;

    //Endpoint para crear un usuario
    @PostMapping
    public ResponseEntity<User> saveUser(@RequestBody User user){
        return userServices.saveUser(user);
    }

    //Endpoint para listar información de un usuario
    @GetMapping("/{id}")
    public Optional<User> getUserId(@PathVariable Long id){
        return userServices.getUserId(id);
    }

    //Endpoint para listar todos los usuarios
    @GetMapping
    public List<User> getAllUsers(){
        return userServices.getUsers();
    }

    //Endpoint para actualizar información de un usuario
    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestBody User user){
        return userServices.updateUser(id, user);
    }
}