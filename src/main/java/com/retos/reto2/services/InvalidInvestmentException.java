package com.retos.reto2.services;

public class InvalidInvestmentException extends RuntimeException {
    public InvalidInvestmentException(String message) {
        super(message);
    }
}