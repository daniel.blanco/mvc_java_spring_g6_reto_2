package com.retos.reto2.services;

import com.retos.reto2.model.User;
import com.retos.reto2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServices {

    private final UserRepository userRepository;

    @Autowired
    public UserServices(UserRepository userRepository){
        this.userRepository =  userRepository;

    }

    //método para listar todos los usuarios
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    // Método para crear un usuario con validación de tipoUsuario
    public ResponseEntity<User> saveUser(User user) {
        // Validar que el tipoUsuario sea "BASICO" o "COMPLETO"
        if (!isValidUserType(user.getTipoUsuario())) {

            return ResponseEntity.badRequest().body(null);
        }

        // Resto de la lógica para guardar el usuario
        if (user.getTipoUsuario().equals("BASICO")){
            user.setIngresoMensual(4000000);
        }else
        {
            user.setIngresoMensual(8500000);
        }
        User savedUser = userRepository.save(user);
        return ResponseEntity.ok(savedUser);
    }

    //método para listar a un usuario por Id
    public Optional<User> getUserId(Long Id){
        return userRepository.findById(Id);
    }

    //método para actualizar a un usuario por Id
    public ResponseEntity<User> updateUser(Long id, User user) {
        Optional<User> existingUser = userRepository.findById(id);

        if (existingUser.isPresent()) {
            User updatedUser = existingUser.get();

            // Validación del tipo de usuario
            if (isValidUserType(user.getTipoUsuario())) {
                updatedUser.setTipoUsuario(user.getTipoUsuario());
                updatedUser.setApellido(user.getApellido());
                updatedUser.setNombre(user.getNombre());
                updatedUser.setDireccion(user.getDireccion());
                updatedUser.setCiudadResidencia(user.getCiudadResidencia());
                updatedUser.setTelefono(user.getTelefono());
                updatedUser.setNumeroDocumento(user.getNumeroDocumento());
                updatedUser.setTipoDocumento(user.getTipoDocumento());


                if (user.getTipoUsuario().equals("BASICO")){
                    updatedUser.setIngresoMensual(4000000);
                }else
                {
                    updatedUser.setIngresoMensual(8500000);
                }
                userRepository.save(updatedUser);
                return ResponseEntity.ok(updatedUser);
            } else {
                // Tipo de usuario no válido
                return ResponseEntity.badRequest().body(null);
            }
        } else {
            // Usuario no encontrado
            return ResponseEntity.notFound().build();
        }
    }

    //método de validación de tipo de usuario
    private boolean isValidUserType(String tipoUsuario) {
        return "BASICO".equals(tipoUsuario) || "COMPLETO".equals(tipoUsuario);
    }

    //método para consultar el ingreso mensual de un usuario
    public double getIngresoMensual(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        return optionalUser.map(User::getIngresoMensual).orElse(0.0);
    }
}
