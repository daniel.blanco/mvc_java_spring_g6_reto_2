package com.retos.reto2.services;

import com.retos.reto2.model.Project;
import com.retos.reto2.model.User;
import com.retos.reto2.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectServices {
    private final ProjectRepository projectRepository;
    @Autowired
    public ProjectServices(ProjectRepository projectRepository){
        this.projectRepository =  projectRepository;
    }

    //método para crear un proyecto:
    public Project saveProject(Project project){
        return projectRepository.save(project);
    }

    //método para buscar un proyecto por ID:
    public Optional<Project> getProjectId(Long Id){
        return projectRepository.findById(Id);
    }

    //método para listar todos los proyectos:
    public List<Project> getProjects(){
        return projectRepository.findAll();
    }

    //método para devolver el monto total a invertir en un proyecto
    public double getCantidadARecaudar(Long projectId) {
        Optional<Project> optionalProject = projectRepository.findById(projectId);
        return optionalProject.map(Project::getCantidadARecaudar).orElse(0.0);
    }
}
