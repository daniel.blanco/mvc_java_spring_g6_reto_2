package com.retos.reto2.services;

import com.retos.reto2.model.Investment;
import com.retos.reto2.model.Project;
import com.retos.reto2.model.User;
import com.retos.reto2.repository.InvestmentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class InvestmentServices {

    private final InvestmentRepository investmentRepository;
    private final ProjectServices projectServices;
    private final UserServices userServices;

    @Autowired
    public InvestmentServices(InvestmentRepository investmentRepository, ProjectServices projectServices, UserServices userServices) {
        this.investmentRepository = investmentRepository;
        this.projectServices = projectServices;
        this.userServices = userServices;
    }

    //método para realizar una inversión de un usuario a un proyecto
    public ResponseEntity<Object> makeInvestment(long userId, long projectId, double amount) {
        try {
            Optional<User> optionalUser = userServices.getUserId(userId);
            Optional<Project> optionalProject = projectServices.getProjectId(projectId);

            if (optionalUser.isPresent() && optionalProject.isPresent()) {
                User user = optionalUser.get();
                Project project = optionalProject.get();

                // Verificar si el proyecto está abierto y si el usuario puede invertir la cantidad
                if (!project.getEstado()) {
                    throw new ProjectClosedException("El proyecto está cerrado, no se puede invertir.");
                }

                if (!isValidInvestment(user, amount, project.getCantidadARecaudar(), project.getCantidadRecaudada())) {
                    throw new InvalidInvestmentException("La inversión no cumple con los requisitos.");
                }

                Investment investment = new Investment();
                investment.setUserId(userId);
                investment.setProjectId(projectId);
                investment.setMontoInvertido(amount);
                investment.setFecha(LocalDate.now());

                // Guardar inversión
                Investment savedInvestment = investmentRepository.save(investment);

                // Actualizar cantidadRecaudada del proyecto
                project.setCantidadRecaudada(project.getCantidadRecaudada() + amount);
                projectServices.saveProject(project);

                return ResponseEntity.ok(savedInvestment);
            }

            // Manejar otras situaciones de error
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error en la inversión.");
        } catch (ProjectClosedException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (InvalidInvestmentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error en la inversión: " + e.getMessage());
        }
    }

    //método de validación de la inversión
    private boolean isValidInvestment(User user, double amount, double projectAmount, double paidAmount ) {
        // Obtener el porcentaje máximo permitido según el tipo de usuario
        double maxPercentage = user.getTipoUsuario().equals("básico") ? 0.20 : 0.25;

        // Calcular el monto máximo permitido para la inversión
        double maxInvestment = user.getIngresoMensual() * maxPercentage;

        // Obtener el acumulado de inversiones actuales del usuario
        double currentInvestments = investmentRepository.sumInvestmentsByUserId(user.getUserId()).orElse(0.0);

        // Verificar si el usuario puede invertir el monto
        if ((currentInvestments + amount) > maxInvestment) {
            throw new InvalidInvestmentException("La inversión supera el máximo permitido para el tipo de usuario.");
        }

        // Verificar si el monto a invertir no supera el máximo de inversión en el proyecto
        if (( paidAmount + amount ) > projectAmount ) {
            throw new InvalidInvestmentException("La inversión supera el monto de inversión del proyecto.");
        }

        return true;
    }

    //método para listar todas la inversiones de un usario
    public List<Investment> getInvestmentsByUserId(long userId){
        return investmentRepository.findByUserId(userId);
    }

    //método para listar todas la inversiones
    public List<Investment> getInvestments(){
        return investmentRepository.findAll();
    }

}
