package com.retos.reto2.services;

public class ProjectClosedException extends RuntimeException {
    public ProjectClosedException(String message) {
        super(message);
    }
}