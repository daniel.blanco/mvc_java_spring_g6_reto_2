package com.retos.reto2;

import com.retos.reto2.model.Project;
import com.retos.reto2.services.ProjectServices;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Reto2Application {


	public static void main(String[] args) {
		SpringApplication.run(Reto2Application.class, args);
	}

	@Bean
	public CommandLineRunner loadData(ProjectServices projectServices) {
		return (args) -> {

			Project project1 = new Project();
			project1.setNombre("Inmobiliaria del Sur");
			project1.setCantidadRecaudada(0);
			project1.setCantidadARecaudar(3000000);
			project1.setEstado(true);
			projectServices.saveProject(project1);

			Project project2 = new Project();
			project2.setNombre("Universidad Tecnológica Lite Thinking");
			project2.setCantidadRecaudada(150000);
			project2.setCantidadARecaudar(3000000);
			project2.setEstado(true);
			projectServices.saveProject(project2);

			Project project3 = new Project();
			project3.setNombre("Urbanizaciones Urubó");
			project3.setCantidadRecaudada(2000000);
			project3.setCantidadARecaudar(3000000);
			project3.setEstado(false);
			projectServices.saveProject(project3);

			System.out.println("Projects initialized");
		};
	}

}
