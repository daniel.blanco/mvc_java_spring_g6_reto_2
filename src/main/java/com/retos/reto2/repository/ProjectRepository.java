package com.retos.reto2.repository;

import com.retos.reto2.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ProjectRepository extends JpaRepository<Project, Long>{
}
