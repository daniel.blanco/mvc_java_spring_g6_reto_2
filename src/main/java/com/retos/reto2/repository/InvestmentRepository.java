package com.retos.reto2.repository;

import com.retos.reto2.model.Investment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

public interface InvestmentRepository extends JpaRepository<Investment, Long> {
    List<Investment> findByUserId(Long userId);
    @Query("SELECT COALESCE(SUM(i.montoInvertido), 0) FROM Investment i WHERE i.userId = :userId")
    Optional<Double> sumInvestmentsByUserId(@Param("userId") long userId);
}
