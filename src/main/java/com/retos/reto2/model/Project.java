package com.retos.reto2.model;

import jakarta.persistence.*;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long projectId;

    @Column()
    private String nombre;
    private double cantidadRecaudada;
    private double cantidadARecaudar;
    private Boolean estado;

    //Getters and Setters
    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getCantidadRecaudada() {
        return cantidadRecaudada;
    }

    public void setCantidadRecaudada(double cantidadRecaudada) {
        this.cantidadRecaudada = cantidadRecaudada;
    }


    public double getCantidadARecaudar() {
        return cantidadARecaudar;
    }

    public void setCantidadARecaudar(double cantidadARecaudar) {
        this.cantidadARecaudar = cantidadARecaudar;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    // Constructores

    public Project() {

    }
}
