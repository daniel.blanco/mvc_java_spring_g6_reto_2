# mvc_java_spring_boot_g6 Reto 2

## Proyecto Registro de Inversiones 
Para el ejercicio se tienen 3 entidades (implementado con BdD H2 en memoria):  
- user
- project
- investment
##### Al ejecutar el proyecto Java Spring boot se cargan datos de test para la tabla project (ver método main de la clase Reto2Application)

## Requisitos
- Java 17
- Maven 3.9.5

## Lista de Endpoints 

### 1. Crear usuarios:  
POST http://localhost:8090/api/v1/users  
Json:
```json
{	
	"nombre": "Daniel",	
	"apellido": "Blanco",	
	"telefono": "456789",
	"direccion": "Direccion #123",
	"ciudadResidencia": "Oruro",
	"tipoDocumento": "P",
	"numeroDocumento": "112233",
	"tipoUsuario": "COMPLETO"
}
```
#### Validaciones: 
- Tipo de Usuario *BASICO* o *COMPLETO*. "200 OK" si cumple sino "400 Bad Request" 
#### Restricciones: 
- Autocompleta el campo ingresoMensual en base al tipo de Usuario. 4 millones COP para "BASICO" Y 8.5 millones COP para "COMPLETO"

### 2. Actualizar usuarios:  
PUT http://localhost:8090/api/v1/users/{id}   
Json:
```json
{	
	"nombre": "Daniel2",	
	"apellido": "Blanco2",	
	"telefono": "4567892",
	"direccion": "Direccion #1232",
	"ciudadResidencia": "Oruro",
	"tipoDocumento": "PC",
	"numeroDocumento": "112232",
	"tipoUsuario": "BASICO"
}
```
#### Restricciones: 
- {id} es el userID de un usuario.
#### Validaciones: 
- TipoUsuario *BASICO* o *COMPLETO*. "200 OK" si cumple sino "400 Bad Request".



### 3. Listar todos los usuarios:  
GET http://localhost:8090/api/v1/users


### 4. Listar Proyectos:
(Se crean 3 al ejecutar el proyecto, ver método main de la clase Reto2Application)

GET http://localhost:8090/api/v1/projects

### 5. Listar Proyecto por Id

GET http://localhost:8090/api/v1/projects/{id}
#### Restricciones: 
- {id} es el projectId de un proyecto.

### 6. Crear proyecto:  

POST http://localhost:8090/api/v1/projects   
Json:
```json
{	
	"nombre": "Nombre Proyecto",	
	"cantidadRecaudada": 0,	
	"cantidadARecaudar": 3000000,
	"estado": true
}
```

### 7. Registrar Inversión

POST http://localhost:8090/api/v1/investments  
Json:
```json
{	
	"userId": 1,	
	"projectId": 1,	
	"montoInvertido": 1000000
}
```
#### Validaciones:
- Registra inversión sólo para proyectos activos (projectId 3 precargado tiene estado false). "200 OK" si cumple sino "400 Bad Request". 
- Registra inversiones de hasta el monto máximo permito para un proyecto verificando todas las inversiones. (campo cantidadARecaudar determina el máximo y cantidadRecaudada el acumulado). Los projectos pre cargados tienen un máximo de 3.0 millones COP. "200 OK" si cumple sino "400 Bad Request". 
- Registra hasta el máximo permitido para un usuario en base al porcentaje de ingreso mensual. (20% de su ingreso mensual para el “Basico” y un máximo del 25% de su ingreso mensual para el “Completo”) "200 OK" si cumple sino "400 Bad Request". 

### 8. Listar todas las Inversiones de todos los Proyectos
GET http://localhost:8090/api/v1/investments

### 9. Listar todas las Inversiones realizadas por un Usuario
GET http://localhost:8090/api/v1/investments/{id}
#### Restricciones: 
- {id} es el userID de un usuario.
